<?php
/**
 * Created by PhpStorm.
 * User: exii
 * Date: 06/08/2016
 * Time: 20:17
 */

namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class IndexController extends AppController
{
    public function index()
    {
        
    }
}

?>